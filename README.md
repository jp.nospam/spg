## Warning

All code in this project is written for educational purposes by an amateur. 

No code was ever audited by any security professionals.

**You should consider this code completely insecure.**

## Stateless Password Generator in C

This project will allow you to compile your own personalized command line 
stateless password generator.

Due to the nature of the project, pre-compiled binaries **shouldn't** and will **never** be 
provided. 

Dependencies: OpenSSL (https://www.openssl.org/)

Adjust personal.h to your liking and then compile.
