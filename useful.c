#include <stdio.h>
#include "useful.h"

void U_ClearFree(void *ptr)
{
        int clean_size = sizeof(ptr);

        memset(ptr, 0, clean_size);
        free(ptr);
}

void U_DisableEcho(void)
{
        #ifdef _WIN32
        HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE); 
        DWORD mode    = 0;
        GetConsoleMode(hStdin, &mode);
        SetConsoleMode(hStdin, mode & (~ENABLE_ECHO_INPUT));
        #endif

        #ifdef __linux__
        struct termios t;

        tcgetattr(STDIN_FILENO, &t);
        t.c_lflag &= ~ECHO;
        tcsetattr(STDIN_FILENO, TCSANOW, &t);
        #endif
}

void U_EnableEcho(void)
{
        #ifdef __linux__
        struct termios t;

        tcgetattr(STDIN_FILENO, &t);
        t.c_lflag |= ECHO;
        tcsetattr(STDIN_FILENO, TCSANOW, &t);
        #endif
}

void U_ToClipboard(char *text)
{
        #ifdef _WIN32
        const size_t len = strlen(text) + 1;

        HGLOBAL hMem = GlobalAlloc(GMEM_MOVEABLE, len);
        memcpy(GlobalLock(hMem), text, len);
        GlobalUnlock(hMem);
        OpenClipboard(0);
        EmptyClipboard();
        SetClipboardData(CF_TEXT, hMem);
        CloseClipboard();
        #endif
}

int U_FindNewLine(char *str)
{
        size_t str_size = strcspn(str, "\n");

        return (int)str_size;
}

void U_Prompt(char *question, char *answer, int a_size)
{
        printf("%s", question);
        while (!fgets(answer, a_size, stdin)) {
                puts("\nInvalid entry.\n");

                printf("%s", question);
        }
        answer[U_FindNewLine(answer)] = 0; // Replace new line with null.
}
