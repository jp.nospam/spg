#ifndef USEFUL_H
#define USEFUL_H

#ifdef _WIN32
#include <windows.h>
#include <process.h>
#endif

#ifdef __linux__
#include <termios.h>
#include <unistd.h>
#endif

#include <stdio.h>

// Disable CLI echo
void U_DisableEcho(void);

// Enable CLI echo
void U_EnableEcho(void);

// Store text to clipboard
void U_ToClipboard(char *text);

// Clear and free used memory
void U_ClearFree(void *ptr);

// Ask a question, get user input, copy input to answer
void U_Prompt(char *question, char *answer, int a_size);

// Find New Line character.
int U_FindNewLine(char *str);

#endif
