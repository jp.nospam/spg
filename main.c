#include <openssl/hmac.h>
#include <openssl/bn.h>
#include "personal.h"
#include "useful.h"

#define FIRST_QUESTION  "Site: "
#define SECOND_QUESTION "Username: "
#define PASS_QUESTION   "Password: "
#define UPPERCASE "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define LOWERCASE "abcedfghijklmnopqrstuvwxyz"
#define NUMBERS   "1234567890"
#define SYMBOLS   "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"
#define SHA256_LEN 64

// Personal options structure.
typedef struct {
        int req_lower;
        int req_upper;
        int req_number;
        int req_symbol;
        char *cus_symbol;
} Options;

// Big Number division structure.
typedef struct {
        BIGNUM *dividend;
        BIGNUM *remainder;
        BIGNUM *divisor;
        BIGNUM *quotient;
        BN_CTX *ctx;
} BN_Division;

// Hex to decimal converter
unsigned long HexToDec(char* hex);

// Quickly test with different character sets
char *BuildCharSet(int lower, int upper, int numbers, int symbols);

// Convert the hash to hex in string format.
char* Hex256Char(int size, unsigned char *d);

// Pick a character and make sure it has required characters
char PickChar(char *char_set, int c_set, Options *options);

// Build password with define character sets.
char BuildPassword(int key_size, char* pass, char* char_set, BN_Division *div, 
                   Options *options);

//  Generate Password
char* GetPassword(char *key, char *char_set, int char_size, int key_size);

// Wrapper for HMAC-SHA256
unsigned char* HmacSha256(const void *key, int keylen, 
                          const unsigned char *data, int datalen, 
                          unsigned char *result, unsigned int* resultlen);

int main(int argc, char **argv)
{
        int key_length = PASSWORD_LENGTH;
        char *char_set = CHAR_SET;
        char *password_key = NULL;
        char first_q[256]  = "";
        char second_q[256] = "";
        char password[256] = "";
       
        if (argc > 2) {
                strcpy(first_q, argv[1]);
                strcpy(second_q, argv[2]);
        } else {
                U_Prompt(FIRST_QUESTION, first_q, 256);
                U_Prompt(SECOND_QUESTION, second_q, 256);
        }
        if (argc > 3)
                strcpy(password, argv[3]);
        else {
                U_DisableEcho(); 
                U_Prompt(PASS_QUESTION, password, 256);
                puts("\n");
                U_EnableEcho();
        }

        char *concat = malloc(256);

        strcpy(concat, first_q);
        strcat(concat, second_q);
        strcat(concat, GEN_SALT);
        memset(first_q, 0, strlen(first_q));
        memset(second_q, 0, strlen(second_q));
        
        unsigned char *hash = HmacSha256(password, strlen(password), 
                                         (const unsigned char *)concat,
                                         strlen(concat), 0, 0);
        char *char_key      = Hex256Char(SHA256_LEN, hash);

        password_key = GetPassword(char_key, char_set, strlen(char_set), 
                                   key_length);
        /*
        #ifdef _WIN32
        ToClipboard(password_key);
        #endif
        */
        printf("%s", password_key);
        U_ClearFree(concat);
        memset(hash, 0, (unsigned char)sizeof(hash)); 
        memset(password, 0, strlen(password));
        memset(char_key, 0, strlen(char_key));
        memset(password_key, 0, strlen(password_key));
        return 0;
}

unsigned long HexToDec(char* hex)
{
        unsigned long x = strtoul(hex, 0, 16);

        return x;
}

char *BuildCharSet(int lower, int upper, int numbers, int symbols)
{
        char *char_set  = (char*)malloc(sizeof(char)*95);

        char_set[0]     = '\0';
        if (upper == 1)
                strcat(char_set, UPPERCASE);
        if (lower == 1)
                strcat(char_set, LOWERCASE);
        if (numbers == 1)
                strcat(char_set, NUMBERS);
        if (symbols == 1)
                strcat(char_set, SYMBOLS);
        return char_set;
}

char PickChar(char *char_set, int c_set, Options *options)
{
        if (options->req_lower == 1) {
                c_set = c_set % strlen(LOWERCASE);
                options->req_lower  = 0;
                return LOWERCASE[c_set];
        }
        if (options->req_upper == 1) {
                c_set = c_set % strlen(UPPERCASE);
                options->req_upper = 0;
                return UPPERCASE[c_set];
        }
        if (options->req_number == 1) {
                c_set = c_set % strlen(NUMBERS);
                options->req_number = 0;
                return NUMBERS[c_set];
        }
        if (options->req_symbol == 1) {
                c_set = c_set % strlen(options->cus_symbol);
                options->req_symbol = 0;
                return options->cus_symbol[c_set];
        }
        return char_set[c_set];
}

char BuildPassword(int key_size, char* pass, char* char_set, 
                   BN_Division *div, Options *options)
{
        if (key_size > 0)
        {
                key_size --;
                pass[key_size] = BuildPassword(key_size, pass, char_set, 
                                               div, options);
        }
        BN_div(div->quotient, div->remainder, div->dividend, div->divisor, 
               div->ctx); 

        char *rem_str = BN_bn2dec(div->remainder);

        BN_clear(div->remainder);

        int c_set   = atoi(rem_str); 
        char letter = PickChar(char_set, c_set, options);

        BN_clear(div->dividend);
        BN_copy(div->dividend, div->quotient);
        BN_clear(div->quotient);

        return letter;
}

char* GetPassword(char *key, char *char_set, int char_size, int key_size)
{
        int count_size = key_size - 1;
        char *c_str    = malloc(sizeof(int));
        int p_size     = (key_size + 1) * sizeof(char);
        char *pass     = malloc(p_size);

        BN_Division div = {BN_secure_new(), BN_secure_new(), 
                           BN_secure_new(), BN_secure_new(), 
                           BN_CTX_secure_new()};
        Options options = {REQUIRE_LOWER, REQUIRE_UPPER, 
                           REQUIRE_NUMBER, REQUIRE_SYMBOL, CUSTOM_SYMBOLS};
        BN_hex2bn(&div.dividend, key); 
        sprintf(c_str, "%d", char_size);
        BN_dec2bn(&div.divisor, c_str);
        memset(pass, 0, p_size);
        pass[count_size] = BuildPassword(count_size, pass, char_set, &div, 
                                         &options);
        BN_clear_free(div.dividend);
        BN_clear_free(div.remainder);
        BN_clear_free(div.divisor);
        BN_clear_free(div.quotient);
        BN_CTX_free(div.ctx);
        return pass;
}

unsigned char* HmacSha256(const void *key, int keylen,
                          const unsigned char *data, int datalen, 
                          unsigned char *result, unsigned int* resultlen)
{
    unsigned char *d = HMAC(EVP_sha256(), key, keylen, data, datalen, result,
                            resultlen);

    return d;
}

char* Hex256Char(int size, unsigned char *d)
{
        char *c = malloc(sizeof(char) * (size * 2));

        for (int i = 0; i < size; i++)
                sprintf(c+2*i, "%02x", d[i]);
        realloc(c, size); // Trim to size.
        c[size] = '\0';
        return c;
}
