#ifndef PERSONAL_H
#define PERSONAL_H

/*
 * PASSWORD_LENGTH      Length of the generated passwords.
 *
 * GEN_SALT             Random string of characters to append to the input for 
 *                      unique generator. **USE YOUR OWN**
 *
 * CHAR_SET             The character set to use when generating a password.
 *
 * REQUIRE_UPPER        If set to 1 then it will force an uppercase alphabet 
 *                      character to be in the generated password.
 *
 * REQUIRE_LOWER        If set to 1 then it will force a lowercase alphabet 
 *                      character to be in the generated password.
 *
 * REQUIRE_NUMBER       If set to 1 then it will force a number character to be 
 *                      in the generated password.
 *
 * REQUIRE_SYMBOL       If set to 1 then it will force a character of 
 *                      CUSTOM_SYMBOLS to be in the generated password.
 *
 * CUSTOM_SYMBOLS       Symbols to pick from if REQUIRE_SYMBOL is set to 1.
 */
#define PASSWORD_LENGTH 20
#define GEN_SALT  "G6_o)30f4Sp-36rjyw^WZ66Q$hq*VN=:a&C[FOde.|]SYvoLAZGX/~)at&0d)uU"
#define CHAR_SET  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcedfghijklmnopqrstuvwxyz1234567890!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"
#define REQUIRE_UPPER  1
#define REQUIRE_LOWER  1
#define REQUIRE_NUMBER 1
#define REQUIRE_SYMBOL 1
#define CUSTOM_SYMBOLS "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"

#endif
